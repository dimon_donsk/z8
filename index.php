<?php
abstract class Products
{
    public $name;
    public $cost;
    public $weight;
    public function __construct($name = 'Продукт',$cost = 'Бесценно',$weight = 'Невесомо')
    {
        $this->name = $name;
        $this->cost = $cost;
        $this->weight = $weight;
    }
    public function printProduct()
    {
        echo "Наименование: $this->name <br>Стоимость: $this->cost <br>Вес: $this->weight <br><br>";
    }
}

class Chocolate extends Products
{
    public $calories;
    public $image;
    public function __construct($name, $cost, $weight, int $calories, string $image)
    {
        $this->calories =$calories;
        $this->image =$image;
        parent::__construct($name, $cost, $weight);
    }
    public function printProduct()
    {
        echo "Наименование: $this->name <br>Стоимость: $this->cost <br>Вес: $this->weight <br> Каллории: $this->calories <br><br>";
        echo "<div>
        <img src='{$this->image}' alt='Шоколадка' width='200' height='200'>
        </div>";
    }
}

class Candy extends Products
{
    public $image;
    public function __construct($name, $cost, $weight, string $image)
    {
        $this->image =$image;
        parent::__construct($name, $cost, $weight);
    }
    public function printProduct()
    {
        echo "Наименование: $this->name <br>Стоимость: $this->cost <br>Вес: $this->weight <br><br>";
        echo "<div>
        <img src='{$this->image}' alt='Конфетка' width='100' height='100'>
        </div>";
    }
}

$chocolad1 = new Chocolate('Шоколад', 60, 0.25, 1500, 'choco.jpg');
$chocolad1->printProduct();
$candy1 = new Candy('Чио Рио', 240, 0.5, 'candy.jpg');
$candy1->printProduct();